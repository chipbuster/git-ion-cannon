# Git Ion Cannon

**This tool is designed to cause data loss. Do not use this program on any production codebase.**

<figure>
<img src="ioncannon.webp"
alt="An image of an ion cannon like the one used in Star Wars Ep. 5" style="width:50%">

<figcaption>Why solve the problem when you can blow it away?</figcaption>

</figure>

## Overview

This tool allows you to replace the contents of a remote git repository hosted
on GitHub or GitLab with the contents of a local repository, bypassing branch
protections if necessary.

Note that this operation inherently causes data loss. Please read the
"Data Safety" section below before using this tool!

## Installation

TODO

## Usage

`git-ion-cannon` requires that your repository is in a clean state: no
uncommited changes, no untracked files, and not currently in the process of
merging, rebasing, or cherry-picking.

There are three things you need to specify to git ion cannon:

- The remote repository you want to replace
- The branches/tags you want to push
- Whether you want to also delete things on the remote

## Rationale

(a.k.a. "Why would you want to do this terrible thing?")

When learning to work with git, it's fairly common to screw up your repository
by committing things you really shouldn't have. As long as the changes stay
local, it's pretty easy to fix: you can use `git reset` to undo bad commits,
or if worst comes to worst, delete your local repository and re-clone it.
However, sometimes the oops goes one level deeper, and you accidentally push
these changes into the remote repository.

Once the remote repository is in a bad state, things get tricky. First off,
nobody else in the group can push until they first pull the changes, which means
dealing with huge, nonsensical merge conflicts. Even if you attempt to
force-push, many student groups do development on protected branches, which
means having to execute a complex dance with the repository settings before you
can push.

I have seen groups spend hours trying to fix this, give up, and just start a
new Github repository using a known good copy of the code. While technically a
valid solution, this is, to put it moderately, a bit of a pain.

This tool attempts to provide a simpler solution by using the GitHub/GitLab APIs
to bypass the worst of the GUI settings dance. From the perspective of trying
to learn git, it's strictly worse than learning how to fix the problem properly,
but it (hopefully) allows groups to save a bunch of time.

## Data Safety

Very approximately, this tool will attempt the following operations:

## Security
